# Label Taxonomy

## Introduction

As the complexity of the GitLab.com application grows, so does the importance of having a consistent, well defined label taxonomy policy on GitLab.com.
Having this in place allows us to better query, analysis, diagnose and find metrics on GitLab.com.

## Standard Labels

| *Label*       | *Description*       |
|---------------|---------------------|
| `environment` | The **environment**. This is the highest label in the hierarchy. In general, each environment should be isolated from the others and share a roughly similar topography.  |
| `tier`        | The **tier**. The tier label defines the major organisational components of the environment. They include: `lb` (load-balancer), `sv` (service), `db` (database), `stor` (storage), `inf` (infrastructure and monitoring). |
| `type`        | The **service**.  A service is a group of nodes and processes that work together to fulfill a function. Many services have multiple components, for example, the `web` service includes `workhorse`, `rails`, `nginx` components amongst other. |
| `stage`       | The **stage**.  The stage is mainly used to differentiate the canary deployment `stage="cny"` from the main production stage `stage="main"`. In general, all nodes in a stage run the same version of the software, but this can be different from other stages. Not all components have multiple stages, in particular, `db` stage services (postgres, redis) that store application state are not tiered. |
| `shard`       | An optional `shard`. A shard can be considered a bulkhead in the application, isolating one part of the application from another. For example, some marquee customers run on an isolated set of Gitaly nodes, and these are differentiated by the `shard` label. |

## Key Metrics

The key metrics that we use for monitoring GitLab.com are aggregated up to the standard labels.

For this reason, it is important that new Prometheus targets are correctly labelled in order to be correctly monitored.

