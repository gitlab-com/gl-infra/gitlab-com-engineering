---
title: Welcome
lang: en-US
---

# GitLab.com Engineering

<iframe src="https://player.vimeo.com/video/143418951" width="560" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/143418951">&quot;View From A Blue Moon&quot; Trailer</a> from <a href="https://vimeo.com/johnjohnflorence">John John Florence</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

<mermaid>
graph TD
  subgraph inf
  monitoring
end
subgraph runners
  ci-runners
end
subgraph stor
  gitaly
  nfs
end
subgraph sv
  api
  git
  registry
  sidekiq
  web
end
api --> gitaly
api --> pgbouncer
api --> redis
api --> redis-cache
api --> redis-sidekiq
ci-runners --> api
git --> gitaly
git --> pgbouncer
git --> redis
git --> redis-cache
git --> redis-sidekiq
registry --> api
sidekiq --> gitaly
sidekiq --> nfs
sidekiq --> pgbouncer
sidekiq --> redis
sidekiq --> redis-cache
sidekiq --> redis-sidekiq
web --> gitaly
web --> pgbouncer
web --> redis
web --> redis-cache
web --> redis-sidekiq
</mermaid>
