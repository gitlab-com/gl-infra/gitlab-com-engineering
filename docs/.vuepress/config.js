"use strict";

const getConfig = require("vuepress-bar");
const barConfig = getConfig(`${__dirname}/..`);

module.exports = {
  title: "GitLab.com Engineering",
  description: "GitLab.com Engineering Site",
  base: "/gl-infra/gitlab-com-engineering/",
  dest: "public",
  themeConfig: {
    repo: "https://gitlab.com/gitlab-com/gl-infra/gitlab-com-engineering",
    docsDir: "docs",
    editLinks: true,
    editLinkText: "Edit this page",
    nav: [
      { text: "Home", link: "/" },
      { text: "Observability", link: "/observability/" },
      { text: "Useful Links", link: "/links/" }
    ],
    sidebar: barConfig.sidebar
  }
};
