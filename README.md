# GitLab.com Engineering Site Source

The source of the GitLab.com Engineering site.

The site is generated using the Vuepress static site generator

## Developing Locally

Be sure to have a recent copy of `node` and `yarn` installed locally, then....

```console
git clone git@gitlab.com:gitlab-com/gl-infra/gitlab-com-engineering.git
yarn install
yarn docs:dev
```
